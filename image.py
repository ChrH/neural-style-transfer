from math import sqrt
import numpy as np
from PIL.ImageStat import Stat
from PIL import Image


def linear_color_histogram_matching(styleimg, contentimg):
    """
    Applies linear color histogram matching as defined in
    "Perceptual Factors in Neural Style Transfer" (Gatys et al. 2017) and references therein.
    Transform the style image to match the mean pixel values and color channel covariance matrix (3x3) of the content image.

    Input: PIL.Image.Image object
    Output: PIL.Image.Image object
    """

    print("Applying color histogram matching to style image.")

    # Scale pixel values to (0, 1)
    styleimg = np.asarray(styleimg)[:,:,:3] / 255. # :3 to ignore alpha channel if it exists
    contentimg = np.asarray(contentimg)[:,:,:3] / 255.

    mean_style = np.mean(styleimg, axis=(0, 1))
    mean_content = np.mean(contentimg, axis=(0, 1))

    # Subtract mean pixel values for each color channel
    styleimg -= mean_style
    contentimg -= mean_content

    # Color channel covariance matrices
    cov_content = 1 / (contentimg.shape[0] * contentimg.shape[1]) * np.einsum('ijk, ijl -> kl', contentimg, contentimg)
    cov_style = 1 / (styleimg.shape[0] * styleimg.shape[1]) * np.einsum('ijk, ijl -> kl', styleimg, styleimg)

    # Eigenvalue decomposition of covariance
    eigval_content, eigvec_content = np.linalg.eig(cov_content)
    eigval_style, eigvec_style = np.linalg.eig(cov_style)

    # Calculate square root for content, inverse square root for style
    sqrt_cov_content = eigvec_content @ np.diag(np.sqrt(eigval_content)) @ eigvec_content.T
    inv_sqrt_cov_style = eigvec_style @ np.diag(1 / np.sqrt(eigval_style)) @ eigvec_style.T
    KK = sqrt_cov_content @ inv_sqrt_cov_style

    # Transform pixel values of style image
    styleimg = np.einsum('ij, abj -> abi', KK, styleimg) + mean_content

    # Clip values, scale back to (0, 255), convert to integer values and save
    styleimg = (np.clip(styleimg, 0., 1.) * 255).astype(np.int8)
    styleimg = Image.fromarray(styleimg, mode='RGB')
    return styleimg


def read_img(imagepath, show=False):
    img = Image.open(imagepath)
    if show:
        print("Size", img.size, "Mode", img.mode)
        img.show()
    return img


def crop_and_resize_img(img, target_ratio=None, maxsize=None, show=False):
    """
    maxsize: maximum number of pixels (by default: output image size)
    target_ratio: shape[0] / shape[1]

    Input: PIL.Image.Image
    Output: PIL.Image.Image
    """
    assert isinstance(img, Image.Image)

    img_size = img.size
    img_ratio = img_size[0] / img_size[1]
    if target_ratio and abs(img_ratio - target_ratio) > 10 ** -5:
        print(f"Cropping image with size {img_size} and ratio {img_ratio} with a target ratio of {target_ratio}")
        if img_ratio > target_ratio:
            width_crop = img_size[0] - img_size[1] * target_ratio
            print("Cropping image width by", width_crop)
            cropwindow = [width_crop // 2, 0, img_size[0] - width_crop // 2, img_size[1]]
        else:
            height_crop = img_size[1] - img_size[0] / target_ratio
            print("Cropping image height by", height_crop)
            cropwindow = [0, height_crop // 2, img_size[0], img_size[1] - height_crop // 2]
        img = img.crop(cropwindow)

    img_size = img.size
    if maxsize:
        scalingfac = maxsize / (img_size[0] * img_size[1])
        if scalingfac < 1:
            size = (int(round(sqrt(scalingfac) * img_size[0], 0)), int(round(sqrt(scalingfac) * img_size[1],0)))
            print(f"Resizing image with size {img.size} to {size}")
            img = img.resize(size, resample=Image.Resampling.LANCZOS)
    if show:
        img.show()
    return img

