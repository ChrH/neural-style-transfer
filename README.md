# Neural Style Transfer
Tensorflow implementation of the original neural style transfer algorithm by [Gatys et al. 2016](https://openaccess.thecvf.com/content_cvpr_2016/html/Gatys_Image_Style_Transfer_CVPR_2016_paper.html) extended with the linear color histogram matching method proposed in [Gatys et al. 2017](https://openaccess.thecvf.com/content_cvpr_2017/html/Gatys_Controlling_Perceptual_Factors_CVPR_2017_paper.html).
It generates an image that combines the style of one image with the content of a second image. These images do not have to be the same size.

# Requirements
- NVIDIA GPU with mixed precision support
- Python >= 3.9
- Tensorflow >= 2.12 (supported on Linux or WSL)
- Pillow >= 9.4

# Basic Usage
`./nst.py -style styleimage.jpg -content contentimage.png -o outputpath -init random`
# Options
Display all options with  `./nst.py -h`
## Important options: 
- `-size`: 2 arguments, width and height of the output image. Default is 1920 x 1080, which requires about 8 GB of VRAM.
- `-colormatch`: Apply linear color histogram matching in order to preserve the color scheme of the content image.
- `-init`: Initialize the output image as random noise (`random`), the content image (`content`), or load an existing image (`/path/to/img.png`) to continue from a previous checkpoint.
- `-outputfreq`: Image output frequency in steps. Images names contain information including the starting time, so no images are overwritten.
- `-contentweight`: Weight factor α of the content loss. The style loss weight is (1 - α). Reasonable values are between 10^-1 and 10^-4.
- `-learningrate`: The choice of the initial learning rate is a tradeoff between noise and convergence speed.
- `-learningdecay`: By default, the learning rate decays exponentially by 0.95 every 100 iterations. Setting this to 1 disables learning rate decay.
