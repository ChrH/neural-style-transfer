#!/usr/bin/env python
"""
Neural style transfer according to
"Image Style Transfer Using Convolutional Neural Networks" (Gatys et al. 2016)
with the option for linear color histogram matching proposed in
"Perceptual Factors in Neural Style Transfer" (Gatys et al. 2017)
"""

import os
import datetime
import argparse
import tensorflow as tf

# local imports
from image import read_img, crop_and_resize_img, linear_color_histogram_matching


def vgg19_model(layer_names, pooling):
    """
    Input: layer_names: list. First style_layer_names, then content_layer_names
    Output: model with inputs being the inputs compatible with VGG19, outputs being
    the selected layers for representing the style and content of images
    """
    vgg = tf.keras.applications.vgg19.VGG19(weights='imagenet', pooling=pooling, include_top=False)
    vgg.trainable = False
    vgg.summary()
    outputs = [tf.cast(vgg.get_layer(i).output, tf.float32) for i in layer_names]
    # print(type(vgg))
    # for i in outputs:
    #     print(i.dtype.name)
    model = tf.keras.Model(inputs=vgg.input, outputs=outputs)
    return model


def preprocess_image_vgg19(img):
    """
    Preprocess: RGB to BGR, zero-center
    Input: Image with pixel values from 0 to 255
    Output: Tensor with each color channel zero-centered
    """
    imgtensor = tf.cast(img, dtype=tf.float32)
    imgtensor = tf.keras.applications.vgg19.preprocess_input(imgtensor)
    imgtensor = tf.expand_dims(imgtensor, axis=0)  # add batch dimension
    return imgtensor


def tensor_to_image(imgtensor):
    shape = tf.shape(imgtensor)
    ndims = tf.shape(shape)
    if ndims > 3:  # first dim is batch size
        assert shape[0] == 1  # only 1 img in batch
        imgtensor = imgtensor[0]
    return tf.keras.preprocessing.image.array_to_img(imgtensor)


def calc_content_loss(outputs_contentimg, outputs_genimg, weights):
    """
    Inputs: lists of output tensors with either 3 dim, or (1, height, width, channels) of the content layers.
            list of weights for each layer
    Output: Squared content loss
    """
    loss = 0.5 / len(outputs_contentimg) * tf.add_n([tf.reduce_sum(k * tf.square(tf.subtract(i, j))) \
               for i, j, k in zip(outputs_contentimg, outputs_genimg, weights)])
    # tf.print(f"CONTENT LOSS:", loss)
    return loss


def calc_style_loss(outputs_styleimg, outputs_genimg, weights):
    """
    Input: lists of outputs from network (style layers only)
           list of weights for each layer
    Output: Squared style loss
    """
    loss = 0.25 / len(outputs_styleimg) * tf.add_n([tf.reduce_mean(k * tf.square(tf.subtract(gram_matrix(i), gram_matrix(j)))) \
                for i, j, k in zip(outputs_styleimg, outputs_genimg, weights)])
    # tf.print(f"STYLE LOSS:", loss)
    return loss


def gram_matrix(T):
    """
    Gram matrix of 4D tensor (see Gatys 2016 paper)
    Feature map F_{ij}^{l} is activation of i-th channel at pos j in layer l
    But input T is in shape (batch, height, width, channel)
    Summation over height and width gives the covariance matrix between channels, the Gram matrix G_{ij}^{l}
    Output: Gram matrix normalized to height * width
    """
    shape = tf.shape(T)
    return tf.einsum("bhwc, bhwd -> bcd", T, T) / tf.cast((shape[1] * shape[2]), tf.float32)


def calc_tot_loss(outputs_styleimg, outputs_contentimg, outputs_genimg, n_stylelayers, style_weights, content_weights, alpha):
    """
    Calculate total loss L = alpha * content_loss + (1 - alpha) * style_loss
    Inputs: Full outputs of style, content, and generated images.
    The style layers are the first n_stylelayers elements in each list
    """
    content_loss = alpha * calc_content_loss(outputs_contentimg[n_stylelayers:], outputs_genimg[n_stylelayers:], content_weights)
    style_loss = (1 - alpha) * calc_style_loss(outputs_styleimg[:n_stylelayers], outputs_genimg[:n_stylelayers], style_weights)
    return content_loss + style_loss


def calc_gradient(genimg, outputs_styleimg, outputs_contentimg, n_stylelayers, style_weights, content_weights, alpha):
    """
    Calculate gradient of loss function wrt image pixel values (width * height * 3 variables)
    """
    with tf.GradientTape() as tape:
        preprocessed_genimg = preprocess_image_vgg19(genimg)
        outputs_genimg = vgg_nst(preprocessed_genimg)
        loss = calc_tot_loss(outputs_styleimg, outputs_contentimg, outputs_genimg, n_stylelayers, style_weights, content_weights, alpha)
    grad = tape.gradient(loss, genimg)
    return loss, grad


@tf.function
def opt_step(genimg, outputs_styleimg, outputs_contentimg, n_stylelayers, style_weights, content_weights, alpha, optimizer):
    """
    Minimize loss of generated image using gradient descent
    """
    loss, gradient = calc_gradient(genimg, outputs_styleimg, outputs_contentimg, n_stylelayers, style_weights, content_weights, alpha)

    # Update generated image according to gradients
    optimizer.apply_gradients([(gradient, genimg)])

    # Clip to range 0, 255
    genimg.assign(tf.clip_by_value(genimg, clip_value_min=0., clip_value_max=255.))
    return loss


if __name__ == '__main__':
    # Mixed precision to use tensor cores
    tf.keras.mixed_precision.set_global_policy('mixed_float16')

    physical_devices = tf.config.list_physical_devices('GPU')
    assert physical_devices, "No GPU found. This would run really, really, unbelievably slowly."

    #if physical_devices:
        # NOTE: set_memory_growth is currently bugged and needs MORE memory
        #tf.config.experimental.set_memory_growth(physical_devices[0], True)
        #gpuinfo = tf.config.experimental.get_memory_info('GPU:0')

    parser = argparse.ArgumentParser(description="Neural style transfer.")
    parser.add_argument('-content', required=True, help='Content image file')
    parser.add_argument('-style', required=True, help='Style image file')
    parser.add_argument('-colormatch', action='store_true', help='Set to transform the style image to match the color of the content image.')
    parser.add_argument('-o', default=None, help='Path for output images. By default, a directory will be created at the current path.')
    parser.add_argument('-init', default='content', help="Optional: Image file as checkpoint/start of optimization. or set to 'random' for "
                                                    "uniform random noise. Default is the content image.")
    parser.add_argument('-niter', default=20000, type=int, help='Number of iterations')
    parser.add_argument('-outputfreq', default=250, type=int, help='Checkpoint frequency')
    parser.add_argument('-learningrate', default=20, type=float, help='Initial learning rate.')
    parser.add_argument('-learningdecay', default=0.95, type=float, help='Decay factor of learning rate per 100 steps (applied continuously). '
                                                                          'Set to 1 to disable learning rate decay.')
    parser.add_argument('-contentweight', default=10 ** -2, type=float, help='Weight of content loss in loss function (alpha). '
                                                                             'Style loss is weighted by (1 - alpha). '
                                                                             'Reasonable values are between 10^-1 and 10^-4.')
    parser.add_argument('-pooling', default='avg', choices=['max', 'avg'], help='Use average or maximum pooling in vgg19.')
    parser.add_argument('-outputformat', default='jpg', choices=['jpg', 'jpeg', 'png'], help='Output image file format')
    parser.add_argument('-size', nargs=2, type=int, default=[1920, 1080], help='2 Arguments: width and height of generated image in '
                                                                               'pixels. Input images are first cropped to correct ratio, '
                                                                               'then scaled to this format.')
    parser.add_argument('-maxsize', type=int, default=None, help='(optional) Maximum number of pixels for style image in case it '
                                                                 'is supposed to be larger than the output image.')
    args = vars(parser.parse_args())


    ALPHA = args['contentweight']
    NEPOCHS = args['niter']
    OUTPUT_FREQ = args['outputfreq']
    OUTPUT_SHAPE = (args['size'][0], args['size'][1])
    stylepath = args['style']
    contentpath = args['content']
    if args['maxsize']:
        MAXSIZE_STYLE = args['maxsize']
    else:
        MAXSIZE_STYLE = OUTPUT_SHAPE[0] * OUTPUT_SHAPE[1]
    RATIO = OUTPUT_SHAPE[0] / OUTPUT_SHAPE[1]

    # Define exponential learning rate decay
    LEARNINGRATE = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=tf.cast(args['learningrate'], tf.float32),
                                                                  decay_steps=100,
                                                                  decay_rate=tf.cast(args['learningdecay'], tf.float32))

    optimizer = tf.keras.optimizers.Adam(learning_rate=LEARNINGRATE)

    # Define output paths and file names
    if args['o']:
        FOLDERNAME = os.path.realpath(args['o'])
        assert os.path.isdir(FOLDERNAME), "-o directory does not exist."
    else:
        FOLDERNAME = f"content_{os.path.split(contentpath)[-1].split('.')[0]}-style_{os.path.split(stylepath)[-1].split('.')[0]}"
        if not os.path.isdir(FOLDERNAME):
            os.mkdir(FOLDERNAME)
    STARTTIME = str(datetime.datetime.now()).split('.')[0].replace(' ', '_').replace(':', '-')
    BASEFILENAME = f"{os.path.split(contentpath)[-1].split('.')[0]}_{OUTPUT_SHAPE[0]}x{OUTPUT_SHAPE[1]}_{STARTTIME}"
    BASEPATH = os.path.join(FOLDERNAME, BASEFILENAME)

    # Read images
    styleimg = read_img(stylepath, show=False)
    contentimg = read_img(contentpath, show=False)

    # Crop to correct ratio, then scale to size
    # Style image: Resize only if the image size is larger than -maxsize, but keep format
    styleimg = crop_and_resize_img(styleimg, maxsize=MAXSIZE_STYLE, show=False)
    # content and generated image must be of same size to calculate the content loss
    contentimg = crop_and_resize_img(contentimg, target_ratio=RATIO, maxsize=OUTPUT_SHAPE[0] * OUTPUT_SHAPE[1], show=False)

    # Optionally apply color transform to style image
    if args['colormatch']:
        styleimg = linear_color_histogram_matching(styleimg, contentimg)

    styleimg_preprocessed = preprocess_image_vgg19(styleimg)
    contentimg_preprocessed = preprocess_image_vgg19(contentimg)

    # Initialize either as content image, random noise, or load existing image
    if args['init'].lower() == 'random':
        print("\nInitializing image with uniform random noise\n")
        genimg = tf.Variable(tf.random.uniform(shape=(OUTPUT_SHAPE[1], OUTPUT_SHAPE[0], 3), minval=0., maxval=255.))
    elif args['init'].lower() == 'content':
        print("\nInitializing image as content image\n")
        genimg = tf.Variable(tf.cast(contentimg, tf.float32))
    else:
        print(f"\nInitializing image by loading file {args['init']}\n")
        genimg = read_img(args['init'])
        genimg = crop_and_resize_img(genimg, target_ratio=RATIO, maxsize=OUTPUT_SHAPE[0] * OUTPUT_SHAPE[1])
        genimg = tf.Variable(tf.cast(genimg, tf.float32))

    # names and relative weights of vgg19 layers (see model summary) to use for style/content
    stylelayer_list = ['block1_conv1',
                       'block2_conv1',
                       'block3_conv1',
                       'block4_conv1',
                       'block5_conv1']
    #style_weights = [1., 0.8, 0.6, 0.4, 0.2]
    style_weights = [1., 1., 1., 1., 1.]
    style_weights = [i / sum(style_weights) for i in style_weights]

    contentlayer_list = ['block5_conv2']
    content_weights = [1.]
    content_weights = [i / sum(content_weights) for i in content_weights]

    layer_list = stylelayer_list + contentlayer_list
    N_STYLELAYERS = len(stylelayer_list)
    N_CONTENTLAYERS = len(contentlayer_list)

    # Create model
    vgg_nst = vgg19_model(layer_list, args['pooling'])

    # Get outputs of style and content image
    outputs_style = vgg_nst(styleimg_preprocessed)
    outputs_content = vgg_nst(contentimg_preprocessed)

    tensor_to_image(genimg).save(f"{BASEPATH}_epoch_000000.{args['outputformat']}")
    print(f"Initial learning rate: {optimizer._current_learning_rate.numpy():.2E}")

    for i in range(NEPOCHS):
        loss = opt_step(genimg, outputs_style, outputs_content, N_STYLELAYERS, style_weights, content_weights, alpha=ALPHA, optimizer=optimizer)
        print(f"Epoch {i + 1:7}\tLoss {loss:.4E}", end='\r', flush=True)
        if (i + 1) % OUTPUT_FREQ == 0:
            print(f"\nWriting checkpoint\tEpoch {i + 1:7}\tLearning rate {optimizer._current_learning_rate.numpy():.2E}")
            img = tensor_to_image(genimg)
            img.save(f"{BASEPATH}_epoch_{i + 1:06}_loss_{loss:.2E}.{args['outputformat']}")
